**ReadMe for DUCK THE VULTURE**


*Duck the Vulture* is a small arcade game, developed with Java 11 and JavaFX.
The drawings for the tokens (duck, vulture, mosquito) I did myself with the
[8bit Painter App](https://play.google.com/store/apps/details?id=com.onetap.bit8painter&hl=de).
The background image comes from [itch.io](https://savvycow.itch.io/loudypixelsky).
The text inserts are made via [textcraft.net](https://textcraft.net/).

Storyline: Harvey the duck has lost sight of his bevy. Now he has to avoid the vultures.
By catching mosquitos he gets extra energy. But this benefit comes with a price...

How to play: Use the arrow keys to move your token (the duck) and avoid hitting
your enemies (the vultures). By catching mosquitos you can earn credits, but
it also inverts the controls every time you catch one.

Try it and have fun.