import javafx.scene.input.KeyCode;
import model.Model;

public class InputHandler {

    private Model model;

    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode keyCode) {
        if (keyCode == KeyCode.UP) {
            model.getDuck().move(0, -10);
        }
        if (keyCode == KeyCode.DOWN) {
            model.getDuck().move(0, 10);
        }
        if (keyCode == KeyCode.LEFT) {
            model.getDuck().move(-10, 0);
        }
        if (keyCode == KeyCode.RIGHT) {
            model.getDuck().move(10, 0);
        }
    }

}
