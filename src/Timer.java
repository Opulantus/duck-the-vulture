import javafx.animation.AnimationTimer;
import model.Model;

import java.io.IOException;

public class Timer extends AnimationTimer {

    private Graphics graphics;
    private Model model;

    private long previousTime = -1;

    public Timer(Graphics graphics, Model model) {
        this.graphics = graphics;
        this.model = model;
    }

    @Override
    public void handle(long nowNano) {      //wird bei jedem Bildaufbau aufgerufen
        long nowMillis = nowNano / 1000000;

        long elapsedTime;
        if (previousTime == -1) {
            elapsedTime = 0;
        } else {
            elapsedTime = nowMillis - previousTime;
        }
        previousTime = nowMillis;
        model.update(elapsedTime);
        try {
            graphics.draw();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
