package model;

import java.util.Random;

public class Vulture {

    private int x;
    private int y;
    private int h;
    private int w;
    private float speedX;

    Random random = new Random();

    public Vulture(int x, int y, float speedX) {
        this.x = x;
        this.y = y;
        this.h = 50;
        this.w = 80;
        this.speedX = speedX;
    }

    /*public void update(long nowNano) {
        this.x = -5 * (Math.round(this.x + nowNano * this.speedX));
        //move(-5, 0);
        /*if(this.x > Model.WIDTH && this.speedX > 0   ||  this.x < 0 && this.speedX < 0 ) {
            this.speedX = -1 * this.speedX;*/


    public void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public void setH(int h) {
        this.h = h;
    }

    public void setW(int w) {
        this.w = w;
    }

    public float getSpeedX() {
        return speedX;
    }
}
