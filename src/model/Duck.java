package model;

public class Duck {

    private int x;
    private int y;
    private int h;
    private int w;
    private boolean drunken;
    private int score;

    public Duck(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 50;
        this.w = 80;
        this.drunken = false;
        this.score = 3;
    }

    public void move(int dx, int dy) {
        if (this.drunken) {
            dx *= (-1);
            dy *= (-1);
        }
        if (this.x + dx >= 0 && this.y + dy >= 0
                && this.x + dx <= 450 && this.y + dy <= 450) {
                this.x += dx;
                this.y += dy;
            }
        }


    public void moveTo(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public boolean isDrunken() {
        return drunken;
    }

    public void setDrunken() {
        this.drunken = !drunken;
    }

    public int getScore() {
        return score;
    }

    public void augScore() {
        this.score += 1;
    }

    public void dimScore() {
        this.score -= 1;
    }
}
