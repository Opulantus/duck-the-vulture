package model;

public class Mosquito extends Vulture {


    public Mosquito(int x, int y, float speedX) {
        super(x, y, speedX);
        super.setH(20);
        super.setW(23);
    }
}
