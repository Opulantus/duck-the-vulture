package model;

import java.util.*;

public class Model {

    private Duck duck;
    private List<Vulture> vultures = new LinkedList<>();
    private List<Mosquito> mosquitos = new LinkedList<>();

    public final double WIDTH = 800;
    public final double HEIGHT = 500;

    private long newWave = 0;

    Random random = new Random();

    public Model() {
        this.duck = new Duck(100, (int) (HEIGHT / 2));
    }

    public void createVultures(int num) {
        for (int i = 1; i <= num; i++) {
            vultures.add(new Vulture(900 + (random.nextInt(200) / i), random.nextInt(400), 0.1f + random.nextFloat() * 0.2f));
            for (int j = 1; j <= num; j++) {
                vultures.add(new Vulture(900 + j * 100 + i * 50, random.nextInt(400), 0.1f + random.nextFloat() * 0.3f));
            }
        }
    }

    public Duck getDuck() {
        return duck;
    }

    public List<Vulture> getVultures() {
        return vultures;
    }

    public List<Mosquito> getMosquitos() {
        return mosquitos;
    }


    public void update(long elapsedTime) {
        newWave -= elapsedTime;
        if (newWave <= 0) {
            createVultures(2); //erzeugt num*num+num Vultures
            newWave = 5000;
        }
        if (newWave == 5000) {
            mosquitos.add(new Mosquito(900, random.nextInt(300), 0.2f + random.nextFloat() * 0.5f));
        }

        for (Mosquito m : mosquitos) {
            m.move((int) (-10 * m.getSpeedX()), 0);
        }
        for (Vulture v : vultures) {
            v.move((int) (-10 * v.getSpeedX()), 0);
        }

        for (Vulture v : vultures) {
            int dx = comparePosition(duck.getX(), v.getX());
            int dy = comparePosition(duck.getY(), v.getY());
            int w = compareSize(duck.getW(), v.getW());
            int h = compareSize(duck.getH(), v.getH());

            if (dx <= w && dy <= h) {
                duck.dimScore();
                duck.moveTo(100, 450);
            }
        }

        for (Mosquito m : mosquitos) {
            int dx = comparePosition(duck.getX(), m.getX());
            int dy = comparePosition(duck.getY(), m.getY());
            int w = compareSize(duck.getW(), m.getW());
            int h = compareSize(duck.getH(), m.getH());

            if (dx <= w && dy <= h) {
                duck.augScore();
                duck.setDrunken();
                mosquitos.remove(m);
            }
        }
    }

    public int comparePosition(int posPlayer, int posEnemy) {
        int d = Math.abs(posPlayer - posEnemy);
        return d;
    }

    public int compareSize(int sizePlayer, int sizeEnemy) {
        int s = sizePlayer / 2 + sizeEnemy / 2;
        return s;
    }


}
