import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import model.Vulture;
import model.Model;
import model.Mosquito;

import javax.swing.*;
import java.io.FileInputStream;
import java.io.IOException;


public class Graphics {

    private GraphicsContext gc;
    private Model model;

    public Graphics(GraphicsContext gc, Model model) {
        this.gc = gc;
        this.model = model;
    }

    public void draw() throws IOException {
        gc.clearRect(0, 0, model.WIDTH, model.HEIGHT);

        Image sky = new Image(new FileInputStream("src/images/Day Sky.png"));
        gc.drawImage(sky, 0, 0, model.WIDTH, model.HEIGHT);

        Image duck = new Image(new FileInputStream("src/images/duck.png"));
        gc.drawImage(duck, model.getDuck().getX(), model.getDuck().getY(),
                model.getDuck().getW(), model.getDuck().getH());

        Image vulture = new Image(new FileInputStream("src/images/vulture.png"));
        for (Vulture v : this.model.getVultures()) {
            gc.drawImage(vulture, v.getX(), v.getY(), v.getW(), v.getH());
        }
        Image mosquito = new Image(new FileInputStream("src/images/mosquito.png"));
        for (Mosquito s : this.model.getMosquitos()) {
            gc.drawImage(mosquito, s.getX(), s.getY(), s.getW(), s.getH());
        }
        JPanel scorePanel = new JPanel();
        gc.setFill(Color.CADETBLUE);
        gc.fillRect(700, 450, 100, 50);
        gc.setFill(Color.BLACK);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setFont(Font.font("Georgia", 20));
        String s = "Score: " + model.getDuck().getScore();
        gc.fillText(s, 750, 480);

    }

}
